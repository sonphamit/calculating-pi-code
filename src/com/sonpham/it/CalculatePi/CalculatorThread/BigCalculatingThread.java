package com.sonpham.it.CalculatePi.CalculatorThread;

import com.sonpham.it.CalculatePi.Common.Constant;

import java.math.BigDecimal;


public class BigCalculatingThread extends CalculatingThread implements Runnable {

    private int decimalDegree;


    //------------ constructor ---------------


    public BigCalculatingThread(long fromIndex, long toIndex, int decimalDegree){
        super(fromIndex,toIndex);
        this.decimalDegree = decimalDegree;
    }

    private boolean running = true;

    // -- result of each thread;


    volatile BigDecimal bigResult = new BigDecimal("0");


    // ---- run method of Runnable.

    @Override
    public void run() {




            BigDecimal i = Constant.ONE;


            for (long n = fromIndex; n < toIndex && running; n++) {


                if (n < 5 * Math.pow(10, decimalDegree - 1) - 1 / 2) {

                    // --- tinh result;



                    BigDecimal resultN = new BigDecimal(n);



                    bigResult = bigResult.add(i.divide(
                            (Constant.TWO.multiply(resultN).add(Constant.ONE))
                            , decimalDegree
                            , BigDecimal.ROUND_HALF_UP));


                    i = i.multiply(Constant.nOne);


                }

            }
            bigResult.setScale(decimalDegree);

           // System.out.println(Thread.currentThread().getName() + " =>  Result = " + bigResult);

        }



    // ---- shutdown method


    @Override
    public void shutdown() {

        running = false;
    }


    // ----- getResult method -> T : BigDecimal.
    @Override
    public BigDecimal getResult() {
        return bigResult;
    }


}
