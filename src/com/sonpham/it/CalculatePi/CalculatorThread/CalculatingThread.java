package com.sonpham.it.CalculatePi.CalculatorThread;

public abstract class CalculatingThread <T> {

    // --- fromIndex

    long fromIndex;

    // --- toIndex

    long toIndex;

    // --- check variable named: running (boolean)

    boolean running;

    public CalculatingThread(long fromIndex, long toIndex) {
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;

    }

    // ------- method

    // --- shutdown method

    public abstract void shutdown();

    public abstract T getResult();


}
