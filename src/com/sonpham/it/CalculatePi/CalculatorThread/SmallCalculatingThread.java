package com.sonpham.it.CalculatePi.CalculatorThread;


public class SmallCalculatingThread extends CalculatingThread implements Runnable {


    private int decimalDegree;



    //------------ constructor  ----------------

    public SmallCalculatingThread(long fromIndex, long toIndex, int decimalDegree){
        super(fromIndex,toIndex);
        this.decimalDegree = decimalDegree;
    }


    /* - volatile dung trong xu ly da luong. vi du khi 10 luong truy cap vao 1 tai nguyen

        thi ben trong jvm se tao 1 ban copy cua result;
    */


    // ------- result variable

    private volatile double result;


    // ------- running variable


    private boolean running = true;




    // --- run method
    @Override
    public void run() {

        int i = 1; // tu so


            for (long n = fromIndex; n < toIndex && running; n++) {


                if (n < 5 * Math.pow(10, decimalDegree - 1) - 1 / 2) {

                    result = result + (double) i / (2 * n + 1);

                    i = i * (-1);
                }

            }

            //System.out.println(Thread.currentThread().getName() + " => Result = " + result);
    }




    // --- getResult method: T -> Double.
    @Override
    public Double getResult() {
        return result;
    }


    // --- Shutdown method.

    @Override
    public void shutdown() {

        running = false;

    }
}
