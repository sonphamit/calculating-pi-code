package com.sonpham.it.CalculatePi;


import com.sonpham.it.CalculatePi.Factory.CalculatorFactory;
import com.sonpham.it.CalculatePi.Interface.Calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;





public class Main {


    // ------------ Main method.

    public static void main(String[] args) {

        int decimalDegree;

        // ---- Enter decimalDegree

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        try {

            System.out.println("Enter decimalDegree : ");


            decimalDegree = Integer.parseInt(br.readLine());


            // ----- create instance of CalculatorFactory.

            CalculatorFactory calculatorFactory = CalculatorFactory.getInstance();



            Calculator calculator = calculatorFactory.getCalculator(decimalDegree);

            // ----- call calculate method.

            calculator.calculate(decimalDegree);


            // ----- call printResult method.

            calculator.printResult();


        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
