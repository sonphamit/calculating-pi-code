package com.sonpham.it.CalculatePi.Factory;

import com.sonpham.it.CalculatePi.Calculator.BigCalculator;
import com.sonpham.it.CalculatePi.Calculator.SmallCalculator;
import com.sonpham.it.CalculatePi.Interface.Calculator;

public class CalculatorFactory{


    // ---------- Singleton -------------

    private CalculatorFactory(){}

    private static class SingletonHelper{
        private static final CalculatorFactory instance = new CalculatorFactory();
    }

    // -------- getInstance method.

    public static CalculatorFactory getInstance(){
        return SingletonHelper.instance;
    }



    // ----- getCalculator (int decimalPoint).

    public Calculator getCalculator(int decimalDegree) {

        if (decimalDegree > 0 && decimalDegree < 17){
            return SmallCalculator.getInstance();
        }
        else{
            return BigCalculator.getInstance();
        }

    }


}
