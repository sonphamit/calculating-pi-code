package com.sonpham.it.CalculatePi.SupportThread;

import com.sonpham.it.CalculatePi.Main;

import java.util.Scanner;


public class ListenerThread extends Thread{


    private ListenerThread(){}

    private static class SingletonHelper{
        private static final ListenerThread instance = new ListenerThread();
    }

    public static ListenerThread getInstance(){
        return SingletonHelper.instance;
    }

    private boolean running = true;


    // ---------- terminate method ------------


    public void terminate(){

        Scanner sc = new Scanner(System.in);

        sc.nextLine();

        running = false;


    }

}

