package com.sonpham.it.CalculatePi.Calculator;

import com.sonpham.it.CalculatePi.CalculatorThread.SmallCalculatingThread;
import com.sonpham.it.CalculatePi.Common.Constant;
import com.sonpham.it.CalculatePi.Interface.Calculator;
import com.sonpham.it.CalculatePi.SupportThread.ListenerThread;


// ---------------- implements Calculator interface -----------------

public class SmallCalculator implements Calculator {

    private SmallCalculator(){}



    private static class SingletonHelperSmallCalculator{
        private static final SmallCalculator instance = new SmallCalculator();
    }

    private boolean running = true;

    public static SmallCalculator getInstance(){
        return SingletonHelperSmallCalculator.instance;
    }


    // --- ResultPI.

    private double resultPI = 0d;



    // --- override calculate method
    @Override
    public void calculate(int decimalDegree) {



        // --- set thread.

        // --- startIndex -- fromIndex;

        long startIndex = 0;

        // --- endIndex -- toIndex;

        long endIndex = Constant.MAX_VALUE_PER_THREAD;

        Thread[] threads = new Thread[Constant.MAX_THREAD];

        SmallCalculatingThread[] ct_small = new SmallCalculatingThread[Constant.MAX_THREAD];



        for (int i = 0; i < Constant.MAX_THREAD; i++) {


                // -- call thread
                ct_small[i] = new SmallCalculatingThread(
                        startIndex, endIndex, decimalDegree);

                // -- ct implements runnable so must call Thread.

                threads[i] = new Thread(ct_small[i]);

                // -- set name thread

                threads[i].setName("Thread " + i);


                // -- start thread


                // ----- set startIndex and endIndex

                startIndex = endIndex;

                endIndex = endIndex + Constant.MAX_VALUE_PER_THREAD;

                threads[i].start();


        }

        for (int i = 0; i < Constant.MAX_THREAD; i++){



            try {

                threads[i].sleep(50);
                

               // ct_small[i].shutdown();


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            break;



        }
        ListenerThread listenerThread = ListenerThread.getInstance();

        listenerThread.terminate();








//        for (int i = 0; i < Constant.MAX_THREAD; i++){
//
//                try {
//
//
//                      threads[i].join();
//
//
//
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//        }


        for (int i =0; i < Constant.MAX_THREAD; i++){

                resultPI += ct_small[i].getResult();

        }




        //---- format ResultPI
        resultPI = Math.round(resultPI * 4 * Math.pow(10, decimalDegree)) / Math.pow(10, decimalDegree);



    }


    // --- override printResult method
    @Override
    public void printResult() {

        System.out.print("\n\n\tPI = " + resultPI + "\n");



    }

    // --- override terminate method

    @Override
    public void terminate() {



    }
}
