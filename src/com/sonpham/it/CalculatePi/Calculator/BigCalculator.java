package com.sonpham.it.CalculatePi.Calculator;

import com.sonpham.it.CalculatePi.CalculatorThread.BigCalculatingThread;
import com.sonpham.it.CalculatePi.Common.Constant;
import com.sonpham.it.CalculatePi.Interface.Calculator;
import com.sonpham.it.CalculatePi.SupportThread.ListenerThread;

import java.math.BigDecimal;

// ------------------ implements Calculator interface ---------------

public class BigCalculator implements Calculator {

    private BigCalculator(){}

    private static class SingletonHelperBigCalculator{
        private static final BigCalculator instance = new BigCalculator();
    }

    public static BigCalculator getInstance(){
        return SingletonHelperBigCalculator.instance;
    }


    BigDecimal resultPI = new BigDecimal("0");



    private static boolean running = true;



    // --- override calculate method
    @Override
    public void calculate(int decimalDegree) {



        // --- startIndex -- fromIndex;

        long startIndex = 0;

        // --- endIndex -- toIndex;

        long endIndex = Constant.MAX_VALUE_PER_THREAD;

        Thread[] threads = new Thread[Constant.MAX_THREAD];

        BigCalculatingThread[] ct_big = new BigCalculatingThread[Constant.MAX_THREAD];

        // --- thread.

        for (int i = 0; i < Constant.MAX_THREAD; i++){

            // -- call thread
            ct_big[i]= new BigCalculatingThread(
                    startIndex,endIndex,decimalDegree);

            // -- ct implements runnable so must call Thread.

            threads[i] = new Thread((Runnable) ct_big[i]);

            // -- set name thread

            threads[i].setName("Thread " + i);

            // -- start thread

//            threads[i].start();



            // ----- set startIndex and endIndex

                startIndex = endIndex;

                endIndex = endIndex + Constant.MAX_VALUE_PER_THREAD;


        }

        for (int i = 0; i < Constant.MAX_THREAD; i++){

            threads[i].start();

            try {

                threads[i].sleep(50);

                ListenerThread listenerThread = ListenerThread.getInstance();

                listenerThread.terminate();

                ct_big[i].shutdown();


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            break;



        }


        // ------ join

        for (int i=0; i < Constant.MAX_THREAD; i++){
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        for (int i=0; i < Constant.MAX_THREAD; i++){
            resultPI = resultPI.add(ct_big[i].getResult());
        }
        // ------ multiply with 4.


        resultPI = resultPI.multiply(Constant.FOUR);


    }


    // --- override printResult method
    @Override
    public void printResult() {

        System.out.println("\n\n\tPi = " + resultPI + "\n");

    }

    // --- override terminate method

    @Override
    public void terminate() {

        running = false;
    }
}
