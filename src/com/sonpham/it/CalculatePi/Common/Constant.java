package com.sonpham.it.CalculatePi.Common;

import java.math.BigDecimal;

public class Constant {


    // ------------- destination Index ----------

    public static final long nSpace = 1000000;

    // ------------- Maximum of thread ------------

    public static final int MAX_THREAD = 100;

    // ------------- Max value per thread ---------

    public static final long MAX_VALUE_PER_THREAD = nSpace / MAX_THREAD;


    public static final BigDecimal ONE = new BigDecimal(1);

    public static final BigDecimal TWO = new BigDecimal(2);

    public static final BigDecimal nOne = new BigDecimal(-1);

    public static final BigDecimal FOUR = new BigDecimal(4);



}
